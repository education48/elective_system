package com.education.electivesystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElectiveSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElectiveSystemApplication.class, args);
	}

}

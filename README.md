It's a simple backend application that works with database.

Description of the subject area:
Elective courses system. There is a list of courses. A teacher is assigned to each of them.
A student enrolls in one or more courses. At the end of the course the teacher gives the student a grade and a feedback.